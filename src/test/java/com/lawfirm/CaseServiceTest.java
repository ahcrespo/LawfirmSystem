package com.lawfirm;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lawfirm.domain.case_.Case;
import com.lawfirm.domain.case_.CaseRepository;
import com.lawfirm.domain.case_.CaseService;
import com.lawfirm.domain.casepermission.CasePermissionRepository;
import com.lawfirm.domain.casepermission.CasePermissionService;
import com.lawfirm.infraestructure.Environment;

import junit.framework.Assert;

public class CaseServiceTest {

	private static final String CRISTINA_K_CASE = "Cristina K";
	private static final String JOSE_LOPEZ_CASE = "Jose Lopez";
	private static final String ABOGADA_HOT = "MAT-1000";
	private static final String ALE_MAGLIETTI = "MAT-2000";
	private static final String SAUL = "MAT-3000";

	private CaseRepository caseRepository;
	private CasePermissionService casePermissionService;
	private CasePermissionRepository casePermissionRepository;
	private CaseService caseService;

	@Before
	public void setUp() {

		caseRepository = Environment.createCaseRepository();
		caseRepository.start();

		casePermissionRepository = Environment.createPermissionRepository();
		casePermissionRepository.start();

		caseService = new CaseService(caseRepository, casePermissionRepository);
		casePermissionService = new CasePermissionService(caseService, casePermissionRepository);

		createDefaultScenario();
	}

	private void createDefaultScenario() {

		Case case1 = Case.createCase(JOSE_LOPEZ_CASE, ABOGADA_HOT);
		Case case2 = Case.createCase(CRISTINA_K_CASE, ABOGADA_HOT);

		caseService.addCase(case1);
		caseService.addCase(case2);
	}

	@After
	public void tearDown() {
		caseRepository.stop();
		casePermissionRepository.stop();
	}

	@Test
	public void findCasesAccessedByALawyerTest() {

		// When
		casePermissionService.grantReadOnlyAccess(ABOGADA_HOT, ALE_MAGLIETTI, JOSE_LOPEZ_CASE);
		casePermissionService.grantReadOnlyAccess(ABOGADA_HOT, ALE_MAGLIETTI, CRISTINA_K_CASE);

		List<Case> cases = caseService.findCasesAccessedBy(ABOGADA_HOT);

		Assert.assertEquals(2, cases.size());
		Assert.assertEquals(JOSE_LOPEZ_CASE, cases.get(0).getCaseId());
		Assert.assertEquals(CRISTINA_K_CASE, cases.get(1).getCaseId());
	}

	@Test
	public void findLawyersWithAccessToCaseTest() {

		// When
		casePermissionService.grantReadOnlyAccess(ABOGADA_HOT, ALE_MAGLIETTI, JOSE_LOPEZ_CASE);
		casePermissionService.grantFullAccess(ABOGADA_HOT, SAUL, JOSE_LOPEZ_CASE);

		List<String> lawyers = caseService.findLawyersWithAccessToCase(JOSE_LOPEZ_CASE);

		Assert.assertEquals(3, lawyers.size());
		Assert.assertEquals(ABOGADA_HOT, lawyers.get(0));
		Assert.assertEquals(ALE_MAGLIETTI, lawyers.get(1));
		Assert.assertEquals(SAUL, lawyers.get(2));
	}
}
