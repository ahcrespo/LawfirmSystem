package com.lawfirm;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lawfirm.domain.case_.Case;
import com.lawfirm.domain.case_.CaseRepository;
import com.lawfirm.domain.case_.CaseService;
import com.lawfirm.domain.casepermission.CasePermissionRepository;
import com.lawfirm.domain.casepermission.CasePermissionService;
import com.lawfirm.infraestructure.Environment;

import junit.framework.Assert;

public class CasePermissionTest {

	private static final String CRISTINA_K_CASE = "Cristina K";
	private static final String SAUL = "MAT-3000";
	private static final String JOSE_LOPEZ_CASE = "Jose Lopez";
	private static final String ABOGADA_HOT = "MAT-1000";
	private static final String ALE_MAGLIETTI = "MAT-2000";

	private CaseRepository caseRepository;
	private CasePermissionService casePermissionService;
	private CasePermissionRepository casePermissionRepository;
	private CaseService caseService;

	@Before
	public void setUp() {

		caseRepository = Environment.createCaseRepository();
		caseRepository.start();

		casePermissionRepository = Environment.createPermissionRepository();
		casePermissionRepository.start();

		caseService = new CaseService(caseRepository, casePermissionRepository);
		casePermissionService = new CasePermissionService(caseService, casePermissionRepository);

		createDefaultScenario();
	}

	@After
	public void tearDown() {
		caseRepository.stop();
		casePermissionRepository.stop();
	}

	/************** PARTICULAR PERMISSIONS ************************/

	/**
	 * Dada una abogada "Hot"
	 * 
	 * Y que tiene asignado el caso "Lopez" y el caso "Cristina"
	 * 
	 * Y una abogada Maglietti
	 * 
	 * Cuando la abogada hot le da permiso de lectura a Maglietti sobre el caso
	 * Lopez
	 * 
	 * Entonces se verifica que Maglietti solo puede acceder al caso Lopez
	 * 
	 */
	@Test
	public void grantReadAccessOnParticularCaseTest() {

		// When
		casePermissionService.grantReadOnlyAccess(ABOGADA_HOT, ALE_MAGLIETTI, JOSE_LOPEZ_CASE);

		// Then
		assertThatAleMagliettiCanAccessToLopezCase();
		assertThatAleMagliettiCannotAccessToCristinaCase();
	}

	/**
	 * Dada una abogada "Hot"
	 * 
	 * Y que tiene asignado el caso "Lopez" y el caso "Cristina"
	 * 
	 * Y una abogada Maglietti
	 * 
	 * Cuando la abogada hot le da permiso de escritura a Maglietti sobre el
	 * caso Lopez
	 * 
	 * Entonces se verifica que Maglietti solo puede acceder y escribir sobre el
	 * caso Lopez
	 * 
	 */
	@Test
	public void granFullAccessOnParticularCaseTest() {

		// When
		casePermissionService.grantFullAccess(ABOGADA_HOT, ALE_MAGLIETTI, JOSE_LOPEZ_CASE);

		// Then
		assertThatAleMagliettiHasFullAccessOnLopezCase();
	}

	/**
	 * Dada una abogada "Hot"
	 * 
	 * Y que tiene asignado el caso "Lopez" y el caso "Cristina"
	 * 
	 * 
	 * Cuando la abogada hot le deniega el acceso a Maglietti sobre el caso
	 * Lopez
	 * 
	 * Entonces se verifica que Maglietti no tiene acceso al caso Lopez
	 * 
	 */
	@Test
	public void denyAccessOnParticularCaseTest() {

		// When
		casePermissionService.denyAccess(ABOGADA_HOT, ALE_MAGLIETTI, JOSE_LOPEZ_CASE);

		// Then
		assertThatAleMagliettiCannotAccessToLopezCase();
	}

	/**
	 * Dada una abogada "Hot"
	 * 
	 * Y que tiene asignado el caso "Lopez" y el caso "Cristina"
	 * 
	 * Y una abogada Maglietti que tiene permisos de escritura sobre el caso
	 * "Lopez"
	 * 
	 * Y un abogado Cuneo
	 * 
	 * Cuando la abogada Maglietti le da permiso de lectura a Cuneo sobre el
	 * caso Lopez
	 * 
	 * Entonces se recibe un mensaje de error indicando que Maglietti no puede
	 * realizar la operacion debido a que no maneja el caso Lopez
	 */
	@Test
	public void particularCasePermissionJustCanBeGrantedByTheCaseManagerTest() {

		// Given
		casePermissionService.grantFullAccess(ABOGADA_HOT, ALE_MAGLIETTI, JOSE_LOPEZ_CASE);

		try {
			casePermissionService.grantReadOnlyAccess(ALE_MAGLIETTI, SAUL, JOSE_LOPEZ_CASE);
			Assert.fail();
		} catch (Exception e) {
			Assert.assertEquals(
					"El abogado: " + ALE_MAGLIETTI
							+ " no puede realizar la operacion debido a que no maneja el caso: Jose Lopez",
					e.getMessage());
		}
	}

	/************** GLOBAL PERMISSIONS ************************/

	/**
	 * Dada una abogada "Hot"
	 * 
	 * Y que tiene asignado el caso "Lopez" y el caso "Cristina"
	 * 
	 * Y una abogada Maglietti
	 * 
	 * Cuando la abogada hot le da a Maglietti permisos globales de lectura
	 * 
	 * Entonces se verifica que Maglietti puede acceder a todos los casos de la
	 * abogada Hot
	 * 
	 */
	@Test
	public void granGlobalReadOnlyAccessTest() {

		// When
		casePermissionService.grantReadOnlyAccess(ABOGADA_HOT, ALE_MAGLIETTI);

		// Then
		assertThatAleMagliettiCanAccessToLopezCase();
		assertThatAleMagliettiCanAccessToCristinaCase();
	}

	/**
	 * 
	 * Dada una abogada "Hot"
	 * 
	 * Y que tiene asignado el caso "Lopez" y el caso "Cristina"
	 * 
	 * Y una abogada Maglietti
	 * 
	 * Cuando la abogada hot le da a Maglietti permisos globales de escritura
	 * 
	 * Entonces se verifica que Maglietti puede escribir en todos los casos de
	 * la abogada Hot
	 * 
	 */
	@Test
	public void granGlobalFullAccessTest() {

		// When
		casePermissionService.grantFullAccess(ABOGADA_HOT, ALE_MAGLIETTI);

		// Then
		assertThatAleMagliettiHasFullAccessOnLopezCase();
		assertThatAleMagliettiHasFullAccessOnCristinaCase();
	}

	/**
	 * Dada una abogada "Hot"
	 * 
	 * Y que tiene asignado el caso "Lopez" y el caso "Cristina"
	 * 
	 * Y una abogada Maglietti
	 * 
	 * Y que la abogada hot ha otorgado permisos globales de escritura a
	 * Maglietti
	 * 
	 * Cuando la abogada hot le da permiso de lectura a Maglietti sobre el caso
	 * Lopez
	 *
	 * Entonces se verifica que Maglietti puede escribir en todos los casos de
	 * la abogada Hot pero solo puede leer el caso Lopez
	 * 
	 */
	@Test
	public void particularPermissionHasPrecedenceOverGlobalPermissions() {

		// Given
		casePermissionService.grantFullAccess(ABOGADA_HOT, ALE_MAGLIETTI);

		// When
		casePermissionService.grantReadOnlyAccess(ABOGADA_HOT, ALE_MAGLIETTI, JOSE_LOPEZ_CASE);

		// Then
		assertThatAleMagliettiCanAccessToLopezCase();
		assertThatAleMagliettiHasFullAccessOnCristinaCase();
	}

	/**
	 * Dada una abogada "Hot"
	 * 
	 * Y que tiene asignado el caso "Lopez" y el caso "Cristina"
	 * 
	 * Y una abogada Maglietti
	 * 
	 * Y que la abogada hot ha otorgado permisos globales de escritura a
	 * Maglietti
	 * 
	 * Cuando la abogada hot le deniega el acceso a Maglietti sobre el caso
	 * Lopez
	 *
	 * Entonces se verifica que Maglietti puede escribir en todos los casos de
	 * la abogada Hot pero no puede acceder al caso Lopez
	 * 
	 */
	@Test
	public void particularPermissionHasPrecedenceOverGlobalPermissionsCase2() {

		// Given
		casePermissionService.grantReadOnlyAccess(ABOGADA_HOT, ALE_MAGLIETTI);

		// When
		casePermissionService.denyAccess(ABOGADA_HOT, ALE_MAGLIETTI, JOSE_LOPEZ_CASE);

		// Then
		assertThatAleMagliettiCannotAccessToLopezCase();
		assertThatAleMagliettiCanAccessToCristinaCase();
	}

	private void assertThatAleMagliettiCannotAccessToLopezCase() {
		Assert.assertFalse(casePermissionService.hasAccessTo(ALE_MAGLIETTI, JOSE_LOPEZ_CASE));
	}

	private void assertThatAleMagliettiCannotAccessToCristinaCase() {
		Assert.assertFalse(casePermissionService.hasAccessTo(ALE_MAGLIETTI, CRISTINA_K_CASE));
	}

	private void assertThatAleMagliettiCanAccessToLopezCase() {
		Assert.assertTrue(casePermissionService.hasAccessTo(ALE_MAGLIETTI, JOSE_LOPEZ_CASE));
		Assert.assertFalse(casePermissionService.hasFullAccessTo(ALE_MAGLIETTI, JOSE_LOPEZ_CASE));
	}

	private void assertThatAleMagliettiCanAccessToCristinaCase() {
		Assert.assertTrue(casePermissionService.hasAccessTo(ALE_MAGLIETTI, CRISTINA_K_CASE));
		Assert.assertFalse(casePermissionService.hasFullAccessTo(ALE_MAGLIETTI, CRISTINA_K_CASE));
	}

	private void assertThatAleMagliettiHasFullAccessOnLopezCase() {
		Assert.assertTrue(casePermissionService.hasFullAccessTo(ALE_MAGLIETTI, JOSE_LOPEZ_CASE));
	}

	private void assertThatAleMagliettiHasFullAccessOnCristinaCase() {
		Assert.assertTrue(casePermissionService.hasFullAccessTo(ALE_MAGLIETTI, CRISTINA_K_CASE));
	}

	private void createDefaultScenario() {

		Case case1 = Case.createCase(JOSE_LOPEZ_CASE, ABOGADA_HOT);
		Case case2 = Case.createCase(CRISTINA_K_CASE, ABOGADA_HOT);

		caseService.addCase(case1);
		caseService.addCase(case2);
	}
}
