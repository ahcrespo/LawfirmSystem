package com.lawfirm.infraestructure;

import com.lawfirm.domain.case_.CaseRepository;
import com.lawfirm.domain.casepermission.CasePermissionRepository;
import com.lawfirm.infraestructure.case_.TransientCaseRepository;
import com.lawfirm.infraestructure.casepermission.TransientCasePermissionRepository;

public class DevEnvironment extends Environment {

	public Boolean isActive() {
		return getCurrentEnvironmentCode().equals(getEnvironmentCode());
	}

	@Override
	public String getEnvironmentCode() {
		return "dev";
	}

	@Override
	public CaseRepository getCaseRepository() {
		return new TransientCaseRepository();
	}

	@Override
	public CasePermissionRepository getCasePermissionRepository() {
		return new TransientCasePermissionRepository();
	}
}
