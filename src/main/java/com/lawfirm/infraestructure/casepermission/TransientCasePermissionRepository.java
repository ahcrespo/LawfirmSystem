package com.lawfirm.infraestructure.casepermission;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.lawfirm.domain.casepermission.CasePermission;
import com.lawfirm.domain.casepermission.CasePermissionRepository;

public class TransientCasePermissionRepository implements CasePermissionRepository {

	private List<CasePermission> casePermissions;

	@Override
	public void start() {
		casePermissions = new ArrayList<>();
	}

	@Override
	public void stop() {
		casePermissions.clear();
	}

	@Override
	public void addOrUpdatePermission(CasePermission newCasePermission) {

		Optional<CasePermission> casePermission = findPermission(newCasePermission);
		if (casePermission.isPresent()) {
			modifyCasePermission(casePermission, newCasePermission);
		} else {
			addCasePermission(newCasePermission);
		}
	}

	private void addCasePermission(CasePermission newCasePermission) {
		casePermissions.add(newCasePermission);
	}

	private void modifyCasePermission(Optional<CasePermission> casePermission, CasePermission newCasePermission) {
		casePermissions.remove(casePermission.get());
		addCasePermission(newCasePermission);
	}

	private Optional<CasePermission> findPermission(CasePermission newCasePermission) {
		return casePermissions.stream().filter(byCaseAndLawyer(newCasePermission)).findAny();
	}

	private Predicate<? super CasePermission> byCaseAndLawyer(CasePermission newCasePermission) {
		return p -> p.hasSameCase(newCasePermission) && p.hasSameLawyer(newCasePermission);
	}

	@Override
	public List<String> findLawyersWithAccessTo(String caseId) {

		return casePermissions.stream().filter(byAccessibleCase(caseId)).map(f -> f.getLawyerId())
				.collect(Collectors.toList());
	}

	private Predicate<? super CasePermission> byAccessibleCase(String caseId) {
		return p -> p.hasCase(caseId) && (p.isAccesible());
	}

	@Override
	public List<String> findCasesAccessedBy(String lawyerId) {

		return casePermissions.stream().filter(byLawyersWithAccess(lawyerId)).map(f -> f.getCaseId())
				.collect(Collectors.toList());
	}

	private Predicate<? super CasePermission> byLawyersWithAccess(String lawyerId) {
		return p -> p.hasLawyer(lawyerId) && (p.isAccesible());
	}

	@Override
	public CasePermission findCasePermission(String caseId, String lawyerId) {
		return casePermissions.stream().filter(p -> p.hasCase(caseId) && p.hasLawyer(lawyerId)).findAny().get();
	}

}
