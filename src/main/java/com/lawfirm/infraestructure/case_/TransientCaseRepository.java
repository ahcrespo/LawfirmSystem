package com.lawfirm.infraestructure.case_;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.lawfirm.domain.case_.Case;
import com.lawfirm.domain.case_.CaseRepository;

public class TransientCaseRepository implements CaseRepository {

	private List<Case> cases;

	@Override
	public void start() {
		cases = new ArrayList<>();
	}

	@Override
	public void stop() {
		cases.clear();
	}

	@Override
	public void addCase(Case _case) {
		getCases().add(_case);
	}

	@Override
	public List<Case> findCases(List<String> caseIds) {

		List<Case> cases = caseIds.stream().map(f -> findCase(f)).collect(Collectors.toList());
		return cases;
	}

	private Case findCase(String caseId) {
		return getCases().stream().filter(byCaseId(caseId)).findFirst().get();
	}

	private Predicate<? super Case> byCaseId(String caseId) {
		return p -> p.getCaseId().equals(caseId);
	}

	@Override
	public List<Case> findCasesManagedBy(String fromLawyerId) {
		return getCases().stream().filter(p -> p.isManagedBy(fromLawyerId)).collect(Collectors.toList());
	}

	private List<Case> getCases() {
		return cases;
	}
}
