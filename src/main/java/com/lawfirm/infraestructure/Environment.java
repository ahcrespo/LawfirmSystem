package com.lawfirm.infraestructure;

import java.util.Arrays;
import java.util.List;
import java.util.PropertyResourceBundle;

import com.lawfirm.domain.case_.CaseRepository;
import com.lawfirm.domain.casepermission.CasePermissionRepository;

public abstract class Environment {

	public abstract Boolean isActive();

	public abstract String getEnvironmentCode();

	public abstract CaseRepository getCaseRepository();

	public abstract CasePermissionRepository getCasePermissionRepository();

	static List<Environment> environments = Arrays.asList(new DevEnvironment());

	public static final CaseRepository createCaseRepository() {
		return findActiveEnvironment().getCaseRepository();
	}

	public static final CasePermissionRepository createPermissionRepository() {
		return findActiveEnvironment().getCasePermissionRepository();
	}

	private static Environment findActiveEnvironment() {
		return environments.stream().filter(p -> p.isActive()).findFirst().get();
	}

	public String getCurrentEnvironmentCode() {
		try {
			return PropertyResourceBundle.getBundle("config").getString("env");
		} catch (Exception e) {
			throw new RuntimeException("Error al procesar el archivo de configuracion");
		}
	}
}
