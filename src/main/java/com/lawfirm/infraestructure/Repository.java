package com.lawfirm.infraestructure;

public interface Repository {

	void start();

	void stop();

}