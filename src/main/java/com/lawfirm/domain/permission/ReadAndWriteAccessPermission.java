package com.lawfirm.domain.permission;

public class ReadAndWriteAccessPermission implements Permission {

	@Override
	public boolean isReadAccessAllowed() {
		return true;
	}

	@Override
	public boolean isWriteAccessAllowed() {
		return true;
	}
}
