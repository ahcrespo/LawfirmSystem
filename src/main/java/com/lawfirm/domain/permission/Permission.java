package com.lawfirm.domain.permission;

public interface Permission {

	boolean isReadAccessAllowed();

	boolean isWriteAccessAllowed();
}
