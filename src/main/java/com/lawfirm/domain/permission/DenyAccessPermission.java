package com.lawfirm.domain.permission;

public class DenyAccessPermission implements Permission {

	@Override
	public boolean isReadAccessAllowed() {
		return false;
	}

	@Override
	public boolean isWriteAccessAllowed() {
		return false;
	}

}
