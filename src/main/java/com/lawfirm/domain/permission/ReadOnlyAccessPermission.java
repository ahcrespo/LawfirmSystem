package com.lawfirm.domain.permission;

public class ReadOnlyAccessPermission implements Permission {

	@Override
	public boolean isReadAccessAllowed() {
		return true;
	}

	@Override
	public boolean isWriteAccessAllowed() {
		return false;
	}
}
