package com.lawfirm.domain.case_;

public class Case {

	private String caseId;
	private String lawyerId;

	public static Case createCase(String caseId, String lawyerId) {
		return new Case(caseId, lawyerId);
	}

	public Case(String caseId, String lawyerId) {
		this.caseId = caseId;
		this.lawyerId = lawyerId;
	}

	public String getCaseId() {
		return caseId;
	}

	public String getLawyerId() {
		return lawyerId;
	}

	public boolean isManagedBy(String lawyerId) {
		return getLawyerId().equals(lawyerId);

	}

	public void syncWith(Case newCase) {

		this.caseId = newCase.getCaseId();
		this.lawyerId = newCase.getLawyerId();
	}

}
