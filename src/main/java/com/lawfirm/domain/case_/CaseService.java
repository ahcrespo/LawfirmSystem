package com.lawfirm.domain.case_;

import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;

import com.lawfirm.domain.casepermission.CasePermission;
import com.lawfirm.domain.casepermission.CasePermissionRepository;
import com.lawfirm.domain.permission.ReadAndWriteAccessPermission;

public class CaseService {

	private CaseRepository caseRepository;
	private CasePermissionRepository casePermissionRepository;

	public CaseService(CaseRepository caseRepository, CasePermissionRepository casePermissionRepository) {
		this.caseRepository = caseRepository;
		this.casePermissionRepository = casePermissionRepository;
	}

	public void addCase(Case lawFirmCase) {
		getCaseRepository().addCase(lawFirmCase);
		grantFullAccessToTheCaseManager(lawFirmCase);
	}

	public List<Case> findCasesAccessedBy(String lawyer) {

		List<String> caseIds = casePermissionRepository.findCasesAccessedBy(lawyer);
		return findCases(caseIds);
	}

	public List<String> findLawyersWithAccessToCase(String caseId) {

		List<String> lawyerIds = casePermissionRepository.findLawyersWithAccessTo(caseId);
		return lawyerIds;
	}

	private void grantFullAccessToTheCaseManager(Case lawFirmCase) {

		CasePermission casePermission = new CasePermission(lawFirmCase.getCaseId(), lawFirmCase.getLawyerId(),
				new ReadAndWriteAccessPermission());

		casePermissionRepository.addOrUpdatePermission(casePermission);
	}

	private CaseRepository getCaseRepository() {
		return caseRepository;
	}

	private List<Case> findCases(List<String> caseIds) {
		return getCaseRepository().findCases(caseIds);
	}

	public void validateIfCaseIsManagedBy(String lawyer, String caseId) {

		List<Case> cases = findCasesManagedBy(lawyer);

		cases.stream().filter(byCaseId(caseId)).findAny().orElseThrow(caseManagerException(lawyer, caseId));
	}

	private Supplier<? extends RuntimeException> caseManagerException(String lawyer, String caseId) {
		return () -> new RuntimeException(
				"El abogado: " + lawyer + " no puede realizar la operacion debido a que no maneja el caso: " + caseId);
	}

	private Predicate<? super Case> byCaseId(String caseId) {
		return p -> p.getCaseId().equals(caseId);
	}

	public List<Case> findCasesManagedBy(String lawyer) {
		return caseRepository.findCasesManagedBy(lawyer);
	}
}
