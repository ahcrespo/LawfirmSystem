package com.lawfirm.domain.case_;

import java.util.List;

import com.lawfirm.infraestructure.Repository;

public interface CaseRepository extends Repository {

	void addCase(Case _case);

	List<Case> findCases(List<String> caseIds);

	List<Case> findCasesManagedBy(String fromLawyerId);

}