package com.lawfirm.domain.casepermission;

import com.lawfirm.domain.permission.Permission;

public class CasePermission {

	private Permission permission;
	private String caseId;
	private String lawyerId;

	public CasePermission(String caseId, String lawyerId, Permission permission) {
		this.caseId = caseId;
		this.lawyerId = lawyerId;
		this.permission = permission;
	}

	public Boolean isWriteAccessAllowed() {
		return permission.isWriteAccessAllowed();
	}

	public boolean isAccesible() {
		return isReadAccessAllowed() || isWriteAccessAllowed();
	}

	public boolean hasCase(String caseId) {
		return this.caseId.equals(caseId);
	}

	public boolean hasLawyer(String lawyerId) {
		return this.lawyerId.equals(lawyerId);
	}

	public boolean hasSameCase(CasePermission newCasePermission) {
		return newCasePermission.hasCase(caseId);
	}

	public boolean hasSameLawyer(CasePermission newCasePermission) {
		return newCasePermission.hasLawyer(lawyerId);
	}

	public String getCaseId() {
		return caseId;
	}

	public String getLawyerId() {
		return lawyerId;
	}

	private Boolean isReadAccessAllowed() {
		return permission.isReadAccessAllowed();
	}
}
