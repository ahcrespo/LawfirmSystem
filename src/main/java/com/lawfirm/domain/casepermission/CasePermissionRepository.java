package com.lawfirm.domain.casepermission;

import java.util.List;

import com.lawfirm.infraestructure.Repository;

public interface CasePermissionRepository extends Repository {

	void addOrUpdatePermission(CasePermission casePermission);

	List<String> findLawyersWithAccessTo(String caseId);

	List<String> findCasesAccessedBy(String lawyerId);

	CasePermission findCasePermission(String caseId, String lawyer);

}