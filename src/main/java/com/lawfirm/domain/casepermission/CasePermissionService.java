package com.lawfirm.domain.casepermission;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import com.lawfirm.domain.case_.Case;
import com.lawfirm.domain.case_.CaseService;
import com.lawfirm.domain.permission.DenyAccessPermission;
import com.lawfirm.domain.permission.Permission;
import com.lawfirm.domain.permission.ReadAndWriteAccessPermission;
import com.lawfirm.domain.permission.ReadOnlyAccessPermission;

public class CasePermissionService {

	private CasePermissionRepository permissionRepository;
	private CaseService caseService;

	public CasePermissionService(CaseService caseService, CasePermissionRepository casePermissionRepository) {
		this.caseService = caseService;
		this.permissionRepository = casePermissionRepository;
	}

	/********** PARTICULAR ACCESS ****************/

	public void grantFullAccess(String fromLawyerId, String toLawyerId, String caseId) {

		CasePermission casePermission = new CasePermission(caseId, toLawyerId, new ReadAndWriteAccessPermission());
		particularAction(caseId, fromLawyerId, toLawyerId, casePermission);
	}

	public void grantReadOnlyAccess(String fromLawyerId, String toLawyerId, String caseId) {

		CasePermission casePermission = new CasePermission(caseId, toLawyerId, new ReadOnlyAccessPermission());
		particularAction(caseId, fromLawyerId, toLawyerId, casePermission);
	}

	public void denyAccess(String fromLawyerId, String toLawyerId, String caseId) {

		CasePermission casePermission = new CasePermission(caseId, toLawyerId, new DenyAccessPermission());
		particularAction(caseId, fromLawyerId, toLawyerId, casePermission);
	}

	/********** GLOBAL ACCESS ****************/

	public void grantFullAccess(String fromLawyerId, String toLawyerId) {

		globalAccessAction(fromLawyerId, toLawyerId, new ReadAndWriteAccessPermission());
	}

	public void grantReadOnlyAccess(String fromLawyerId, String toLawyerId) {
		globalAccessAction(fromLawyerId, toLawyerId, new ReadOnlyAccessPermission());
	}

	public void denyAccess(String fromLawyerId, String toLawyerId) {
		globalAccessAction(fromLawyerId, toLawyerId, new DenyAccessPermission());
	}

	/****************************************/

	private void particularAction(String caseId, String fromLawyerId, String toLawyerId,
			CasePermission casePermission) {

		validateIfCaseIsManagedBy(fromLawyerId, caseId);

		getPermissionRepository().addOrUpdatePermission(casePermission);
	}

	private void globalAccessAction(String fromLawyerId, String toLawyerId, Permission permission) {

		List<Case> cases = findCasesManagedBy(fromLawyerId);
		List<String> caseIds = toString(cases);

		caseIds.forEach(caseId -> particularAction(caseId, fromLawyerId, toLawyerId,
				new CasePermission(caseId, toLawyerId, permission)));
	}

	private void validateIfCaseIsManagedBy(String lawyer, String caseId) {

		caseService.validateIfCaseIsManagedBy(lawyer, caseId);

		if (!findCasesManagedBy(lawyer).stream().anyMatch(p -> p.getCaseId().equals(caseId)))
			throw new RuntimeException("El abogado: " + lawyer
					+ " no puede realizar la operacion debido a que no maneja el caso: " + caseId);
	}

	private List<Case> findCasesManagedBy(String fromLawyerId) {
		return caseService.findCasesManagedBy(fromLawyerId);
	}

	private List<String> toString(List<Case> cases) {
		return cases.stream().map(c -> c.getCaseId()).collect(Collectors.toList());
	}

	private CasePermissionRepository getPermissionRepository() {
		return permissionRepository;
	}

	public Boolean hasAccessTo(String lawyerId, String lawCaseFirm) {

		try {
			CasePermission casePermission = permissionRepository.findCasePermission(lawCaseFirm, lawyerId);
			return casePermission.isAccesible();
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public Boolean hasFullAccessTo(String lawyerId, String lawCaseFirm) {

		try {
			CasePermission casePermission = permissionRepository.findCasePermission(lawCaseFirm, lawyerId);
			return casePermission.isWriteAccessAllowed();
		} catch (NoSuchElementException e) {
			return false;
		}

	}

}
