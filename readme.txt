Estudio de abogados

Enunciado

Tenemos un sistema para estudios de abogados, en el que los abogados manejan sus casos. Cada abogado puede darle permisos a otros miembros de su estudio para que puedan acceder a sus casos. 
Los permisos que da un abogado pueden ser de lectura o acceso total (lectura y escritura). El abogado que maneja un caso siempre tiene acceso total a el, y es el único que puede dar permisos a otros abogados sobre ese caso.

Los permisos pueden ser globales (para todos los casos que maneja el abogado) o particulares (para un caso especifico). Los permisos particulares de estar definidos, tienen precedencia por sobre los permisos globales.

Por ejemplo Juan le da a Pedro acceso de lectura para todos sus casos, pero también le da accedo de escritura para el caso "A". Entonces Pedro puede leer todos los casos de Juan, pero el caso "A" también lo puede escribir. 
Otro ejemplo puede ser: Juan le da a Pedro acceso de lectura en todos sus casos, pero le deniega el acceso al caso "A", con lo cual Pedro puede leer todos los casos de Juan pero no puede acceder al caso "A".

Lo que se quiere entonces es:
-Permitir a un abogado darle permisos a otro miembro de su firma para todos sus casos
-Permitir a un abogado darle o denegarle permisos a otro miembro de su firma para un caso particular
-Conocer todos los casos a los que puede acceder un abogado
-Conocer todos los abogados que pueden acceder a un caso
-Autorizar o rechazar el acceso de lectura a un abogado sobre un caso
-Autorizar o rechazar el acceso total a un abogado sobre un caso

Aclaraciones:
Podes usar cualquier lenguaje de programacion
No hay que desarrollar el user interface.
Debe tener test.
Desarrollarlo usando TDD preferentemente. 
No es necesario implementar persistencia
